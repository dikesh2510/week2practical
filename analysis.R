df <- read.csv("DATASET.csv")

filter_df <- subset(df, yearOfRegistration >= 1950 & yearOfRegistration <= 2000 & price >= 10000 & price <= 100000)

x <- as.numeric(filter_df$price)
y <- as.numeric(filter_df$yearOfRegistration)

cor.test(x, y, method="pearson")
